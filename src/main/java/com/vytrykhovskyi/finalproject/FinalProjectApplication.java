package com.vytrykhovskyi.finalproject;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class FinalProjectApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(FinalProjectApplication.class)
                .logStartupInfo(false)
                .run(args);
    }

}

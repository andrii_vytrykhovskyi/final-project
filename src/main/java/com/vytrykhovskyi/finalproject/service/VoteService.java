package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.dto.VoteDto;
import com.vytrykhovskyi.finalproject.exceptions.PostNotFoundException;
import com.vytrykhovskyi.finalproject.exceptions.SpringBlogException;
import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.Vote;
import com.vytrykhovskyi.finalproject.repository.PostRepository;
import com.vytrykhovskyi.finalproject.repository.VoteRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.vytrykhovskyi.finalproject.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
@Slf4j
public class VoteService {

    private final PostRepository postRepository;
    private final VoteRepository voteRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        Post post = postRepository.findById(voteDto.getPostId())
                .orElseThrow(() -> {
                    log.error("Post not found with id - " + voteDto.getPostId() + " to vote");
                    return new PostNotFoundException("Post Not Found with ID - " + voteDto.getPostId());
                });
        Optional<Vote> voteByPostAndUser = voteRepository.findTopByPostAndUserOrderByVoteIdDesc(post, authService.getCurrentUser());
        if (voteByPostAndUser.isPresent() &&
                voteByPostAndUser.get().getVoteType()
                        .equals(voteDto.getVoteType())) {
            throw new SpringBlogException("You have already "
                    + voteDto.getVoteType() + "'d for this post");
        }
        if (UPVOTE.equals(voteDto.getVoteType())) {
            post.setVoteCount(post.getVoteCount() + 1);
        } else {
            post.setVoteCount(post.getVoteCount() - 1);
        }
        log.info("Post " + post.getPostId() + " was voted");
        voteRepository.save(mapToVote(voteDto, post));
        postRepository.save(post);
    }

    private Vote mapToVote(VoteDto voteDto, Post post) {
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .post(post)
                .user(authService.getCurrentUser())
                .build();
    }
}

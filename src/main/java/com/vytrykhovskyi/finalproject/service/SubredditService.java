package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.dto.SubredditDto;
import com.vytrykhovskyi.finalproject.exceptions.SpringBlogException;
import com.vytrykhovskyi.finalproject.mapper.SubredditMapper;
import com.vytrykhovskyi.finalproject.model.Subreddit;
import com.vytrykhovskyi.finalproject.repository.SubredditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class SubredditService {

    private final SubredditRepository subredditRepository;
    private final SubredditMapper subredditMapper;

    @Transactional
    public SubredditDto save(SubredditDto subredditDto) {
        Subreddit subreddit = subredditRepository.save(subredditMapper.mapDtoToSubreddit(subredditDto));
        subredditDto.setId(subreddit.getId());
        log.info("Subreddit was created");
        return subredditDto;
    }

    @Transactional
    public List<SubredditDto> getAll() {
        log.info("Getting all subrredits");
        return subredditRepository.findAll()
                .stream()
                .map(subredditMapper::mapSubredditToDto)
                .collect(toList());
    }

    public SubredditDto getSubreddit(Long id) {
        Subreddit subreddit = subredditRepository.findById(id)
                .orElseThrow(() -> {
                    log.error("No subreddit found with id + " + id);
                    return new SpringBlogException("No subreddit found with ID - " + id);
                });
        log.info("Getting subreddit with id " + id);
        return subredditMapper.mapSubredditToDto(subreddit);
    }
}

package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.dto.CommentsDto;
import com.vytrykhovskyi.finalproject.exceptions.PostNotFoundException;
import com.vytrykhovskyi.finalproject.mapper.CommentMapper;
import com.vytrykhovskyi.finalproject.model.Comment;
import com.vytrykhovskyi.finalproject.model.NotificationEmail;
import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.User;
import com.vytrykhovskyi.finalproject.repository.CommentRepository;
import com.vytrykhovskyi.finalproject.repository.PostRepository;
import com.vytrykhovskyi.finalproject.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class CommentService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;
    private final MailContentBuilder mailContentBuilder;
    private final MailService mailService;


    public void save(CommentsDto commentsDto) {
        Post post = postRepository.findById(commentsDto.getPostId())
                .orElseThrow(() -> {
                    log.error("Post for comment not found " + commentsDto.getPostId().toString());
                    return new PostNotFoundException(commentsDto.getPostId().toString());
                });
        Comment comment = commentMapper.map(commentsDto, post, authService.getCurrentUser());
        commentRepository.save(comment);
        log.info("User " + commentsDto.getUserName() + " left comment " + commentsDto.getText()
                + " under post " + commentsDto.getPostId());
        String message = mailContentBuilder.build(post.getUser().getUsername() + " posted a comment on your post.");
        sendCommentNotification(message, post.getUser());
    }

    private void sendCommentNotification(String message, User user) {
        mailService.sendMail(new NotificationEmail(user.getUsername() + " Commented on your post", user.getEmail(), message));
        log.info("Email with notification about " + user.getUsername() + "`s comment sent");
    }

    public List<CommentsDto> getAllCommentsForPost(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> {
                    log.error("Post with id " + postId + " for getting all comments not found");
                    return new PostNotFoundException(postId.toString());
                });
        log.info("Getting all comment for post with id " + postId);
        return commentRepository.findByPost(post)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<CommentsDto> getAllCommentsForUser(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> {
                    log.error("User with username " + userName + " for getting all comments not found");
                    return new UsernameNotFoundException(userName);
                });
        log.info("Getting all comment for user with username " + userName);
        return commentRepository.findAllByUser(user)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(Collectors.toList());
    }
}

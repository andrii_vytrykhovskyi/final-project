package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.dto.PostRequest;
import com.vytrykhovskyi.finalproject.dto.PostResponse;
import com.vytrykhovskyi.finalproject.exceptions.PostNotFoundException;
import com.vytrykhovskyi.finalproject.exceptions.SubredditNotFoundException;
import com.vytrykhovskyi.finalproject.mapper.PostMapper;
import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.Subreddit;
import com.vytrykhovskyi.finalproject.repository.PostRepository;
import com.vytrykhovskyi.finalproject.repository.SubredditRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class PostService {

    private final SubredditRepository subredditRepository;
    private final AuthService authService;
    private final PostMapper postMapper;
    private final PostRepository postRepository;

    public void save(PostRequest postRequest) {
        Subreddit subreddit = subredditRepository.findByName(postRequest.getSubredditName())
                .orElseThrow(() -> {
                    log.error("Subreddit with name " + postRequest.getSubredditName() + "not found for post creation");
                    return new SubredditNotFoundException(postRequest.getSubredditName());
                });
        log.info("Post " + postRequest.getPostName() + " was saved");
        postRepository.save(postMapper.map(postRequest, subreddit, authService.getCurrentUser()));
    }

    @Transactional(readOnly = true)
    public PostResponse getPost(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> {
                    log.error("No post with id " + id + " to get");
                    return new PostNotFoundException(id.toString());
                });
        log.info("Getting post with id " + id);
        return postMapper.mapToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAllPosts() {
        log.info("Getting all posts");
        return postRepository.findAll()
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }
}
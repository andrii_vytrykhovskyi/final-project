package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.dto.AuthenticationResponse;
import com.vytrykhovskyi.finalproject.dto.LoginRequest;
import com.vytrykhovskyi.finalproject.dto.RefreshTokenRequest;
import com.vytrykhovskyi.finalproject.dto.RegisterRequest;
import com.vytrykhovskyi.finalproject.exceptions.SpringBlogException;
import com.vytrykhovskyi.finalproject.model.NotificationEmail;
import com.vytrykhovskyi.finalproject.model.User;
import com.vytrykhovskyi.finalproject.model.VerificationToken;
import com.vytrykhovskyi.finalproject.repository.UserRepository;
import com.vytrykhovskyi.finalproject.repository.VerificationTokenRepository;
import com.vytrykhovskyi.finalproject.security.JwtProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static com.vytrykhovskyi.finalproject.util.Constants.ACTIVATION_EMAIL;

@Service
@AllArgsConstructor
@Slf4j
public class AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final MailService mailService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final RefreshTokenService refreshTokenService;


    @Transactional
    public void signup(RegisterRequest registerRequest) {
        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setEmail(registerRequest.getEmail());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        user.setCreated(Instant.now());
        user.setEnabled(false);
        userRepository.save(user);
        log.info("User registration Successful");
        String token = generateVerificationToken(user);
        mailService.sendMail(new NotificationEmail("Please activate your account", user.getEmail(),
                "Thank you for signing up to Spring Blog, please click on the below url to activate " +
                        "your account : " + ACTIVATION_EMAIL + "/" + token));
    }

    private String generateVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);
        log.info("Verification token was generated for user " + user.getUsername());
        verificationTokenRepository.save(verificationToken);
        return token;
    }

    public void verifyAccount(String token) {
        Optional<VerificationToken> verificationToken = verificationTokenRepository.findByToken(token);
        verificationToken.orElseThrow(() -> {
            log.error("Invalid Token");
            return new SpringBlogException("Invalid Token");
        });
        fetchUserAndEnable(verificationToken.get());
        log.info("Account Activated Successfully");
    }

    @Transactional(readOnly = true)
    User getCurrentUser() {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        return userRepository.findByUsername(principal.getUsername())
                .orElseThrow(() -> {
                    log.error("Username not found " + principal.getUsername());
                    return new UsernameNotFoundException("Username not found - " + principal.getUsername());
                });
    }

    @Transactional
    void fetchUserAndEnable(VerificationToken verificationToken) {
        String username = verificationToken.getUser().getUsername();
        User user = userRepository.findByUsername(username).orElseThrow(() -> {
            log.error("User not found with name " + verificationToken.getUser().getUsername());
            return new SpringBlogException("User Not found with username - " + username);
        });
        user.setEnabled(true);
        log.info("User " + verificationToken.getUser().getUsername() + " is activated");
        userRepository.save(user);
    }

    public AuthenticationResponse login(LoginRequest loginRequest) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        String token = jwtProvider.generateToken(authenticate);
        log.info("User " + loginRequest.getUsername() + " was logged in");
        return AuthenticationResponse.builder()
                .authenticationToken(token)
                .refreshToken(refreshTokenService.generateRefreshToken().getToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationTime()))
                .username(loginRequest.getUsername())
                .build();
    }

    public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
        String token = jwtProvider.generateTokenWithUserName(refreshTokenRequest.getUsername());
        log.info("Token was refreshed for user " + refreshTokenRequest.getUsername());
        return AuthenticationResponse.builder()
                .authenticationToken(token)
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationTime()))
                .username(refreshTokenRequest.getUsername())
                .build();
    }
}

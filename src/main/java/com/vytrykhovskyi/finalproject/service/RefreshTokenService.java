package com.vytrykhovskyi.finalproject.service;

import com.vytrykhovskyi.finalproject.exceptions.SpringBlogException;
import com.vytrykhovskyi.finalproject.model.RefreshToken;
import com.vytrykhovskyi.finalproject.repository.RefreshTokenRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;

    public RefreshToken generateRefreshToken() {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken.setCreatedDate(Instant.now());
        log.info("Refresh token was generated");
        return refreshTokenRepository.save(refreshToken);
    }

    void validateRefreshToken(String token) {
        refreshTokenRepository.findByToken(token)
                .orElseThrow(() -> {
                    log.error("Invalid refresh token");
                    return new SpringBlogException("Invalid refresh token");
                });
        log.info("Refresh token was validated");
    }

    public void deleteRefreshToken(String token) {
        refreshTokenRepository.deleteByToken(token);
        log.info("Refresh token was deleted");
    }
}

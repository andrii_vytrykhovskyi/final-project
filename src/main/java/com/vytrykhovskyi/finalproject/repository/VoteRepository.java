package com.vytrykhovskyi.finalproject.repository;

import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.User;
import com.vytrykhovskyi.finalproject.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}

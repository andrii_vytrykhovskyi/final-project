package com.vytrykhovskyi.finalproject.repository;

import com.vytrykhovskyi.finalproject.model.Comment;
import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);
}

package com.vytrykhovskyi.finalproject.repository;

import com.vytrykhovskyi.finalproject.model.Post;
import com.vytrykhovskyi.finalproject.model.Subreddit;
import com.vytrykhovskyi.finalproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);
}

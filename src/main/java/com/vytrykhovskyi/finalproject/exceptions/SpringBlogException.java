package com.vytrykhovskyi.finalproject.exceptions;

public class SpringBlogException extends RuntimeException {
    public SpringBlogException(String s) {
        super(s);
    }
}
